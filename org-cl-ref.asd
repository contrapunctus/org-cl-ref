(defsystem     "org-cl-ref"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Generate Org reference documentation for Common Lisp code"
  :serial      t
  :depends-on  (:definitions :cl-ppcre :serapeum :trivia :trivial-types)
  :components  ((:module "src/"
                 :components ((:file "package")
                              (:file "core")))))
