(in-package :org-cl-ref)

#+(or)
(defun location-to-link (location)
  "Return LOCATION as a link in Org markup."
  (uiop:enough-pathname
   location
   ;; We are in doc/, but need the relative pathname from the project root
   (uiop:pathname-parent-directory-pathname *default-pathname-defaults*)))

(defun format-definition-type (definition)
  (let ((type (def:type definition)))
    (apply #'format nil ":~@{~(~A~):~^~}"
           (serapeum:split-sequence #\- (symbol-name type)
                                    :remove-empty-subseqs t))))

(defun format-direct-superclasses (definition)
  (mapcar #'class-name
          (cm:class-direct-superclasses
           (find-class
            (def:designator definition)))))

(defvar *definition-id-alist* nil
  "Alist of definitions and their CUSTOM_ID properties as a string.
Set to NIL once export is completed.")

(defun format-properties (definition id-prefix)
  "Return properties for a heading as a string.

Generate a CUSTOM_ID property based on ID-PREFIX and the name of
DEFINITION."
  (let ((custom-id (format nil "~(~A~A~)" id-prefix (def:name definition))))
    ;; (push (cons definition custom-id) *definition-id-alist*)
    (format nil ":PROPERTIES:~%:CUSTOM_ID: ~A~%:END:~%"
            custom-id)))

;; Find identifiers enclosed within ` and '.
;; If identifiers are defined elsewhere in the document, link to them.
(defun format-docstring (definition)
  (let ((pat    "`(.+?)'")
        (docstring (def:documentation definition)))
    (re:do-register-groups (identifier) (pat docstring docstring)
      (setf docstring
            (re:regex-replace pat docstring
                              (format nil "=~A=" identifier))))
    ;; Unwrap hard-wrapped lines
    (re:regex-replace-all "([^\\n])\\n([^\\n])" docstring "\\1 \\2")))

(defun format-definitions (designators depth id-prefix)
  "Generate reference documentation for DESIGNATORS, as Org markup.

Each element of DESIGNATORS can be a string, a symbol, or a list
like (setf car) - see `definitions:find-definitions'.

An element of DESIGNATORS may also be a list with one of the
aforementioned objects as the first element, followed by one or more
of the keyword arguments `:package' and `:type'. The values to the
keyword arguments must be acceptable to
`definitions:find-definitions'.

An Org heading is created for each element of DESIGNATORS - DEPTH
should be a positive integer to determine the depth of each heading.

A CUSTOM_ID property is defined for each heading based on the
definition name and ID-PREFIX, which should be a string ending in the
desired separator."
  (loop :for des :in designators
        :for def = (cond ((and (listp des) (tt:property-list-p (rest des)))
                          (first (apply #'def:find-definitions des)))
                         (t (let ((package (if (symbolp des)
                                               (symbol-package des)
                                               *package*)))
                              (first (def:find-definitions des :package package)))))
        :for index :from 1
        :when def
          :collect
          (let ((name (def:name def))
                ;; (location (def:source-location def))
                (arguments (pat:match (def:type def)
                             ;; Prefix names of direct superclasses with package names
                             ('class (format nil "~:S" (format-direct-superclasses def)))
                             ('variable nil)
                             ((or 'function 'macro-function 'generic-function)
                              ;; Print () if no arguments, or print
                              ;; the argument list while omitting
                              ;; package names
                              (format nil "~:[()~;~:*~A~]" (def:arguments def)))))
                (docstring (format-docstring def)))
            (format nil "~A =~(~A~@[ ~A~]~)= ~A~%~A~@[  ~A~%~%~]"
                    (make-string depth :initial-element #\*)
                    name
                    arguments
                    (format-definition-type def)
                    (format-properties def id-prefix)
                    docstring))
          :into strings
        :else :do (error (format nil "No definition found for ~A" des))
        :finally
           (return (apply #'format nil "~@{~A~}" strings))))
