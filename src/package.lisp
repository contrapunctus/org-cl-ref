(defpackage :org-cl-ref
  (:use :cl)
  (:local-nicknames (:def :definitions)
                    (:re :ppcre)
                    (:s :split-sequence)
                    (:cm :closer-mop)
                    (:pat :trivia)
                    (:tt :trivial-types))
  (:export :format-definitions))
